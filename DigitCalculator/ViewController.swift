
//  ViewController.swift
//  DigitCalculator
//
//  Created by Reena Singh on 2/9/15.
//  Copyright (c) 2015 ccsf. All rights reserved.

/* DigitCalculator:
Has 'one' outlet: Display and 'seven' action are buttons of digit 1,2,3 addition +,
Subtraction -, Clear c, and Total =.
*/

/*Instruction to run:

Simply click run triangular button, Simulator will pop up with iphone 6 version whichever is set
on your machine. There is text box, and some digit on the DigitCalculator. The text box will displays 
the number clicked as well as runningTotal. Run the calculator as regular calculator.

&&&&****Special Instruction***&&&&

Please click in sequence as follows:=

Test case: 1
Addition : 1 + 2 + = 3

Test case: 2
Addition : 1 + 2 + 3 + = 6

Test case 3:

Addition & subtraction : 1 + 2 + 3 + 3 - 2 - 1 - 1 + = 1

Test case 4:

subtraction : 2 - 1 - 3 - 2 + = 0

Test case 5:

clear : click c, display is empty
*/

import UIKit

class ViewController: UIViewController {
    
   
    @IBOutlet weak var Display: UITextField!
    
    // initialize of global variables
    var one = 1
    var two = 2
    var three = 3
    var sumValue = 0
    var subValue = 0
    var temp = 0
    var runningTotal = 0
    var someInts = [Int]()
    var lastCompute = 0
    
    // on clicking button '1' this action is performed
    @IBAction func oneClicked(_ sender: UIButton) {
        
        // one has value of '1' as string
        Display.text = "\(one)"
        let tmpOne = Display.text
        
        // Convert string'1' to integer value 1
        temp = (tmpOne! as NSString).integerValue
        print("Value oneClicked = \(temp)\n")
        
    }
    
    // on clicking button '2' this action is performed
    @IBAction func twoClicked(_ sender: UIButton) {
        
        // one has value of '2' as string
        Display.text = "\(two)"
        let tmpTwo = Display.text
        
        // Convert string'2' to integer value 2
        temp = (tmpTwo! as NSString).integerValue
        print("Value twoClicked = \(temp)\n")
        
    }
  
    // on clicking button '3' this action is performed
    @IBAction func threeClicked(_ sender: AnyObject) {
        
        // one has value of '3' as string
        Display.text = "\(three)"
        let tmpThree = Display.text
        
        // Convert string '3' to integer value 3
        temp = (tmpThree! as NSString).integerValue
        print("Value threeClicked = \(temp)\n")
        
    }
    
    // on clicking button '+' sum action is performed
    @IBAction func sumClicked(_ sender: UIButton) {
        
        print("Addition Clicked:= \n")
        
        //temp is assign a value either by button 1 | 2 | 3 on click
        let y = temp
        
       /*Initially someInts array is empty, that time value lastComputer is assign zero
        otherwise if someInts[] is filled, we pick that last value which is running total
        in calculation and assign that value to last compute.*/
        
        if(!someInts.isEmpty){
            lastCompute = someInts.last!
        }
        
        print("runningTotal before func call:sum = \(runningTotal)\n")
        
        //function sum is called, to add to running total new temp value clicked
        runningTotal = sum(lastCompute, y: y)
        
        // new runningTotal
        print("runningTotal after func call:sum = \(runningTotal)\n")
        
        // Display's new runningtotal on display
        Display.text = "\(runningTotal)"
        
        // As many times new temp clicked, the new runningTotal is calculated with sum func and 
        // is assign to the someInt[] array. This array is passed on between two function sum and sub
        someInts += [runningTotal]
        
        print(someInts)
        print(someInts.last!)
    }
    
    // on clicking button '-' sub action is performed
    @IBAction func subClicked(_ sender: UIButton) {
        
        //temp is assign a value either by button 1 | 2 | 3 on click
        print("Subtraction Clicked:= \n")
        let y = temp
        
        if(!someInts.isEmpty){
            lastCompute = someInts.last!
        }
       
        print("runningTotal before func call:sub = \(runningTotal)\n")
        
        //function sub is called, to add to running total new temp value clicked
        runningTotal = sub(lastCompute , y: y)
        
        // new runningTotal
        print("runningTotal after func call:sub = \(runningTotal)\n")
        
        // Display's new runningtotal on display
        Display.text = "\(runningTotal)"
        
        // As many times new temp clicked, the new runningTotal is calculated with sum func and
        // is assign to the someInt[] array. This array is passed on between two function sum and sub
        someInts += [runningTotal]
        print(someInts)
        print(someInts.last!)
        
        
        
    }
    
    // on clicking button 'c' clear action is performed
    @IBAction func clearClicked(_ sender: UIButton) {
        
        print("Clear Clicked:= all values are zero\n")
        temp = 0
        sumValue = 0
        subValue = 0
        Display.text = " "
        runningTotal = 0
        
        //initialize to empty array
        someInts = [Int]()
        
    }
    
    // on clicking button '=' 'total value computed to point' action is performed
    @IBAction func equalClicked(_ sender: UIButton) {
        
        runningTotal = nowTotal(runningTotal)
        print("runningTotal after func call:nowTotal = \(runningTotal)\n")
        Display.text = "\(runningTotal)"
        
    }
    // func sum takes two parameter x,y coming from action(+) clicked
    // y is coming from temp on click of 1|2|3
    // x is runningTotal
    func sum(_ x:Int, y:Int)->Int{
        
        var someValue = 0
        
        // someInts array is empty. so someValue gets 'value' coming from y (on digit clicked)
        if(someInts.isEmpty){
            
            someValue = y
        }
        else { // someValue gets value of runningTotal + new digit clicked(1|2|3)
            
            someValue = x
            someValue += y
        }
        
        print("sum func return someValue = \(someValue) x = \(x) sumValue = \(sumValue)\n")
        
        return someValue
        
    }
    
    // func sub takes two parameter x, y coming from action(-) clicked
    // y is coming from temp on click of 1|2|3
    // x is runningTotal
    func sub(_ x:Int, y:Int)->Int{
        
        var someValue = 0
        
        // someInts array is empty. so someValue gets 'value' coming from y (on digit clicked)
        if(someInts.isEmpty){
            
            print("someInts array is empty, therefore x is initial value clicked\n")
            someValue = y
        }
        
        if(!someInts.isEmpty){
            
            // someValue gets value of runningTotal + new digit clicked(1|2|3)
            someValue = x
            someValue = someValue - y
            
        }
        
        print("sub func returns someValue = \(someValue) x = \(x) subValue = \(subValue)\n")
        return someValue
    }
    
    //func nowTotal takes one parameter from Total, which is runningTotal in action(=) clicked
    func nowTotal(_ Total:Int)->Int{
        
        runningTotal = Total
        print("Total = \(Total) runningTotal = \(runningTotal)\n")
        return Total
        
    }
    
}


